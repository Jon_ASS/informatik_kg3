#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <thread>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
using namespace std;


int fibonacci(int n)
{
	if (n < 3) return 1;
	return fibonacci(n - 1) + fibonacci(n - 2);
}


void call_from_thread1()
{
	int result = fibonacci(42);
	cout << result << endl;
}


void call_from_thread2()
{
	int Zaehler = 0;
	for(;;){
		pid_t x = syscall(__NR_gettid);
    	cout << "Die Thread-ID lautet: " <<  x << endl;
    	cout << Zaehler << endl;
    	Zaehler++;	
    	sleep(0.1);
	}
}


int main ()
{
	char Eingabe;
	thread t1(call_from_thread1);
	thread t2(call_from_thread1);
	thread t3(call_from_thread1);
	thread t4(call_from_thread1);
	thread t5(call_from_thread2);
	thread t6(call_from_thread2);

	// Es werden die beiden Zähler bis unendlich gestartet, da aber die 
	// anderen Programme im thread weiter laufen und durch die Verteilungsstelle
	// Arbeitsspeicher bekommen, werden die Ergebnisse davon irgendwann in die
	// Konsole geschrieben. Dadurch, dass aber die Ergebnisse so schnell durch die 
	// anderen Ausgaben verschwinden bemerken wir sie nicht.
	// Die beiden Zähler zählen in etwa gleicher Geschwindigkeit, dass bedeutet das
	// beide die gleiche Zeit vom Verteiler bekommen.
	

    cout << "Drücken sie Enter zum weiter machen." << endl;
    cin.get();

    return 0;
}