#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <thread>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <mutex>
using namespace std;

mutex m;


int fibonacci(int n)
{
	if (n < 3) return 1;
	return fibonacci(n - 1) + fibonacci(n - 2);
}


void call_from_thread1()
{
	m.lock();
	int result = fibonacci(42);
	cout << result << endl;
	m.unlock();
}


void call_from_thread2()
{
	m.lock();
	int Zaehler = 0;
	for(;;){
		pid_t x = syscall(__NR_gettid);
    	cout << "Die Thread-ID lautet: " <<  x << endl;
    	cout << Zaehler << endl;
    	Zaehler++;	
    	sleep(0.1);
	}
	m.unlock();
}


int main ()
{
	char Eingabe;
	thread t1(call_from_thread1);
	thread t2(call_from_thread1);
	thread t3(call_from_thread1);
	thread t4(call_from_thread1);
	thread t5(call_from_thread2);
	thread t6(call_from_thread2);


	t1.join();
	t2.join();
	t3.join();
	t4.join();

	// Hier wird bei jedem Thread darauf gewartet, dass der andere Fertig ist.
	// Dabei ist dass Problem, dass durch den Mutex Schlüssel nur einer gleichzeitig
	// ablaufen kann. Aber es gibt uns die Zeit um zu sehen, dass die anderen threads
	// auch etwas ausgeben.
	//
	// Durch den Mutex Schlüssel geht dabei aber der Vorteil von mehreren threads verloren
	// und der zweite Zähler startet nie.
	

    cout << "Drücken sie Enter zum weiter machen." << endl;
    cin.get();

    return 0;
}