#include <iostream>
#include <chrono>
#include <thread>
#include <cstdio>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

using namespace std;


void call_from_thread1(int &Eingabe)
{
	if(Eingabe < 100){
		cout << "Die Zahl lautet:" << Eingabe << endl;
		Eingabe++;
	}
}

void call_from_thread2(int &Eingabe)
{
	if(Eingabe > 0){
		cout << "Die Zahl lautet:" << Eingabe << endl;
		Eingabe--;
	}
}


int main()
{
	int Eingabe;
	cout << "Herzlich Willkommen" << endl;
	cout << "Bitte gebe eine ganzzahlige Zahl zwischen 0 und 100 ein." << endl;
	cin >> Eingabe;

	thread t1(call_from_thread1, ref(Eingabe));
	sleep(1);
	thread t2(call_from_thread1, ref(Eingabe));

	t1.join();
	t2.join();

	cout << "Drücken sie Enter zum weiter machen." << endl;
    cin.ignore(42, '\n');
    cin.get();

	return 0;
}