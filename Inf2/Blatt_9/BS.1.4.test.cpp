#include <iostream>
#include <thread>

using namespace std;

// Diese Methode ist die Thread-Methode, d.h. alles was in ihr steht
// wird als Thread ausgefuehrt.
// Mehrere Threads koennen diese Methode unabhaengig voneinander
// gleichzeitig benutzen.

void call_from_thread()
{
	cout << "Hello, World" << endl;
}

int main()
{
	// Der zusaetzliche Thread t1 wird erzeugt und gestartet.
	thread t1(call_from_thread);

	// Die main-Methode wartet an dieser Stelle auf die Beendigung von t1
	// thread und main werden wieder zusammengefuehrt.
	t1.join();

	return 0;
}