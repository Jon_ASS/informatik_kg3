#include <iostream>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
using namespace std;
using namespace chrono;

// Funktion um eine Fibonacci-Folge zu bererchnen
int fibonacci(int n)

{
	if (n < 3) return 1;
	return fibonacci(n - 1) + fibonacci(n - 2);
}


void call_from_thread1()
{
	// Hier werden wie in der main-Funktion auf dem main Kern zwei Zeitpunkte erstellt. Dabei laufen jetzt beide Programme
	// auf zwei Unterschiedlichen Kernen gleichzeitig ab.
	time_point<system_clock> start2, end2;


	// start-Zeitpunkt die aktulle Uhrzeit zuweisen (augelagert im Kern)
	start2 = system_clock::now();

	// Fibonaccizahlen berechnen (augelagert im Kern)
	int result2 = fibonacci(42);

	// end-Zeitpunkt die aktuelle Uhrzeit zuweisen (augelagert in weiterem Kern)
	end2 = system_clock::now();


	int elapsed_seconds2 = duration_cast<microseconds>(end2 - start2).count();

	cout << "finished computation, elapsed time: thread:" << elapsed_seconds2 << "mys" << endl;
	cout << "result thread:" << result2 << endl;

}


int main()
{
	char Eingabe;

	thread t1(call_from_thread1);
	// In einem weiteren Kern wird das Programm call_from_thread1 ausgeführt, dabei geht die main-Funktion wie zuvor
	// weiter.
	// Zwei Zeitpunkte zur Zeitmessung
	time_point<system_clock> start, end;


	// start-Zeitpunkt die aktulle Uhrzeit zuweisen (main- Kern)
	start = system_clock::now();

	// Fibonaccizahlen berechnen (main- Kern)
	int result = fibonacci(42);

	// end-Zeitpunkt die aktuelle Uhrzeit zuweisen (main- Kern)
	end = system_clock::now();

	t1.join();
	// Die main-Funktion wartet hier bis der ausgelagerte Prozess im anderen Kern zu ende ist. Die Zeit für die 
	// Fibonacci Folge betrifft das nicht. 

	// Berechnung der benoetigten Sekunden
	int elapsed_seconds = duration_cast<microseconds>(end - start).count();

	// Ausgabe
	cout << "finished computation, elapsed time: main:" << elapsed_seconds << "mys" << endl;
	cout << "result main:" << result << endl;	
	// Nach dem nun beide Prozesse beendet wurden und eine bestimmte Zeit ausgegeben haben, kann diese jetzt 
	// Verglichen werden. Dabei habe ich zur besseren Veranschaulichung die beiden Werte in Micro-Sekunden dargestellt.
	// Dabei fällt erst einmal die zu erwartende Ählichkeit der beiden Werte auf, dies liegt daran das beide Prozesse 
	// zeitgleich auf unterschiedlichen Kernen abläuft. Und deswegen jedes Programm die gleiche Prozessor Kapazität besitzt.
	// Erst nach mehr maligem Ausführen fällt einem Auf, dass die Zeit um einen gewissen Wert schwankt und keiner der beiden
	// Proesse jemals wirklich schneller ausgefürt wird.

	// Blockieren des Prozesses
	cin.get();

	return 0;
}