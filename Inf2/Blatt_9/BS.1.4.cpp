#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
using namespace std;

void call_from_thread1()
{
	for (int i = 1; i < 31; i++){
		cout << i << endl;
		pid_t x = syscall(__NR_gettid);
    	cout << "Die Thread-ID lautet: " <<  x << endl;
    	sleep(1);
	}
}


void call_from_thread2()
{
	pid_t x = syscall(__NR_gettid);
    cout << "Die Thread-ID lautet: " <<  x << endl;
}


int main ()
{
	char Eingabe;
	thread t1(call_from_thread1);
	thread t2(call_from_thread2);

	
	t2.join();
	cout << "Der zweite Thread wurde beendet." <<  endl;

	t1.join();
    cout << "Der erste Thread wurde beendet." << endl;
	

    cout << "Drücken sie Enter zum weiter machen." << endl;
    cin.get();

    return 0;
}