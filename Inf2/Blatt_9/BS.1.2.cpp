#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
using namespace std;


int main(int argc, char* argv[ ])
{
	char Eingabe;
    cout << "Die Thread-ID lautet: " <<  pthread_self() << endl;
    pid_t x = syscall(__NR_gettid);
    cout << "Die Thread-ID lautet: " <<  x << endl;
    cout << "Drücken sie Enter zum weiter machen." << endl;
    cin.get();
    return 0;
}