#include <iostream>
#include "Complex.h"

using namespace std;

int main()
{

    //Test Addition
    Complex z1 = Complex(8, 7);
    Complex z2 = Complex(5, 5);
    Complex z3 = z1 + z2;
    cout << "Realteil: " << z3.getrealPart() << " , Imaginärteil: " << z3.getimaginaerPart() << endl;
    cout << z3 << endl;

    //Test Subtraktion
    z3 = z1 - z2;
    cout << "Realteil: " << z3.getrealPart() << " , Imaginärteil: " << z3.getimaginaerPart() << endl;
    cout << z3 << endl;

    //Test Input und Output
    cin >> z3;
    cout << z3 << endl;

    //Test Division 
    cin >> z1;
    cin >> z2;
    z3 = z1 / z2;
    cout << z3 << endl;

    //Test Teilen durch 0
    z2 = Complex(0, 0);
    try
    {
        z3 = z1 / z2;
    }
    catch (exception e)
    {
        cout << "Fehler: " << e.what() << endl;
    }

}
