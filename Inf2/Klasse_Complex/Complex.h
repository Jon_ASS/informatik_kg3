#pragma 
#include <iostream>


class Complex
{
private:

	double realPart;
	double imaginaerPart;

public:
	Complex(double real, double imaginaer);

	void setrealPart(double real);
	void setimaginaerPart(double imaginaer);

	double getrealPart();
	double getimaginaerPart();

	Complex operator+(Complex& z);
	Complex operator-(Complex& z);
	
};
// Überladung der Operatoren << und >>
std::ostream& operator<< (std::ostream& a, Complex& z);
std::istream& operator>>(std::istream& a, Complex& z);

// globale Funktion: Divisionsoperator Complex
Complex operator/(Complex& z1, Complex& z2); 
