#include "Complex.h"
#include <iostream>

using namespace std;

Complex::Complex(double real, double imaginaer)
{
	realPart = real;
	imaginaerPart = imaginaer;
}

void Complex::setrealPart(double real)
{
	realPart = real;
}

void Complex::setimaginaerPart(double imaginaer)
{
	imaginaerPart = imaginaer;
}

double Complex::getrealPart()
{
	return realPart;
}

double Complex::getimaginaerPart()
{
	return imaginaerPart;
}

Complex Complex::operator+(Complex& z)
{
	return Complex(realPart + z.getrealPart(), imaginaerPart + z.getimaginaerPart());
}

Complex Complex::operator-(Complex& z)
{
	return Complex(realPart - z.getrealPart(), imaginaerPart - z.getimaginaerPart());
}

// Operatoren Überladung
ostream& operator<< (ostream& a, Complex& z)
{
	a << z.getrealPart() << (z.getimaginaerPart() >= 0.0 ? "+" : "") << z.getimaginaerPart() << "i";
	return a;

}

istream& operator>>(istream& a, Complex& z)
{
	double input;
	cout << "Realteil: ";
	a >> input;
	z.setrealPart(input);

	cout << "Imaginärteil: ";
	a >> input;
	z.setimaginaerPart(input);

	return a;
}

// globale Funktion: Divisionsoperator Complex
Complex operator/(Complex& z1, Complex& z2)
{
	//bezeichnung aus dem Skript übernommen
	double a = z1.getrealPart();
	double b = z1.getimaginaerPart();
	double c = z2.getrealPart();
	double d = z2.getimaginaerPart();
	
	// ungültiges Argument, wenn c und d gleich null sind
	if (c == 0 && d == 0)
	{
		throw invalid_argument("Division mit 0");
	}

	double r = (a * c + b * d) / (c * c + d * d);
	double i = (b * c - a * d) / (c * c + d * d);

	return Complex(r,i);	
}







