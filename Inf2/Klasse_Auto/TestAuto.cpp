#include "Klasse_Auto.h"
#include "Klasse_Auto.cpp"
#include<iostream>
#include<string>
using namespace std;

int main()
{

	//Test Standardkonstruktor auf Stack
	Auto Testauto_1 = Auto();

	//Test allgemeiner Konstruktor auf Datensegment
	static Auto Testauto_2 = Auto(4, 200, 2, 45.0, 45.0, 8, "rot");

	//Test Kopierkonstruktor auf Heap
	Auto* Testauto_3 = new Auto(Testauto_2);

	cout << "--Test der Konstruktoren und Methoden--" << endl;
	cout << "Testauto 1:\nRadzahl: " << Testauto_1.getradzahl() << " PS: " << Testauto_1.getps() << " Sitze: " << Testauto_1.getanzahl_sitze() <<  
	" Tankinhalt: " << Testauto_1.gettankinhalt() << "l Tankgroesse: " << Testauto_1.gettankgroesse() << "l Spritverbrauch: " << 
	Testauto_1.getspritverbrauch() << "l/100km Farbe: " << Testauto_1.getfarbe() << endl; 

	cout << "Testauto 2:\nRadzahl: " << Testauto_2.getradzahl() << " PS: " << Testauto_2.getps() << " Sitze: " << Testauto_2.getanzahl_sitze() <<  
	" Tankinhalt: " << Testauto_2.gettankinhalt() << "l Tankgroesse: " << Testauto_2.gettankgroesse() << "l Spritverbrauch: " << 
	Testauto_2.getspritverbrauch() << "l/100km Farbe: " << Testauto_2.getfarbe() << endl; 

	cout << "Testauto 3:\nRadzahl: " << Testauto_3->getradzahl() << " PS: " << Testauto_3->getps() << " Sitze: " << Testauto_3->getanzahl_sitze() <<  
	" Tankinhalt: " << Testauto_3->gettankinhalt() << "l Tankgroesse: " << Testauto_3->gettankgroesse() << "l Spritverbrauch: " << 
	Testauto_3->getspritverbrauch() << "l/100km Farbe: " << Testauto_3->getfarbe() << endl; 

	cout << "--Test des Destruktors--" << endl;
	delete Testauto_3;
 	cout << "Wenn nach dem Doppelpunkt keine Farbe steht, dann funktioniert der Destruktor:" << Testauto_3->getfarbe() << " ;)" << endl;
	
	return 0;
}