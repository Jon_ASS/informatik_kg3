#pragma once
#include<iostream>
#include<string>

using namespace std;

class Auto 
{

private:

	int radzahl;
	int ps;
	int anzahl_sitze;
	float tankinhalt;
	float tankgroesse;
	float spritverbrauch;
	string farbe;
	
public:

	float fahren(float strecke);

//Standardkonstruktor
	Auto();

//allgemeiner Konstruktor
	Auto(int Radzahl, int PS, int Anzahl_Sitze, float Tankinhalt, float Tankgroesse, float Spritverbrauch, string Farbe);

//Kopierkonstruktor
	Auto(const Auto & c);

//Destruktor
	~Auto();

//Getter
	int getradzahl();
	int getps();
	int getanzahl_sitze();
	float gettankinhalt();
	float gettankgroesse();
	string getfarbe();
	float getspritverbrauch();

//Setter
	float settankinhalt(float menge);
};
