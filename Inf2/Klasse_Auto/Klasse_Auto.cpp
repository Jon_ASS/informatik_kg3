#include "Klasse_Auto.h"
#include<iostream>
#include<string>
using namespace std;


//Standardkonstruktor
Auto::Auto()
{
	radzahl = 4;
	ps = 150;
	anzahl_sitze = 5;
	tankinhalt = 60.0;
	tankgroesse = 60.0;
	spritverbrauch = 5;
	farbe = "weiss";
}

//allgemeiner Konstruktor
Auto::Auto(int Radzahl, int PS, int Anzahl_Sitze, float Tankinhalt, float Tankgroesse, float Spritverbrauch, string Farbe)
{
	radzahl = Radzahl;
	ps = PS;
	anzahl_sitze = Anzahl_Sitze;
	tankinhalt = Tankinhalt;
	tankgroesse = Tankgroesse;
	spritverbrauch = Spritverbrauch;
	farbe = Farbe;
}

//Kopierkonstruktor
Auto::Auto(const Auto & c)
{
	radzahl = c.radzahl;
	ps = c.ps;
	anzahl_sitze = c.anzahl_sitze;
	tankinhalt = c.tankinhalt;
	tankgroesse = c.tankgroesse;
	farbe = c.farbe;
	spritverbrauch = c.spritverbrauch;
}

//Destruktor
Auto::~Auto(){}

float Auto::fahren(float strecke)
{
    float maxReichweite = (tankinhalt / spritverbrauch) * 100.0f;
    if (strecke > maxReichweite){
        tankinhalt = 0;
        return maxReichweite;
    }
	float verbrauchStrecke = (strecke/100.0f) * spritverbrauch;
	tankinhalt -= verbrauchStrecke;
	return strecke;
}

//Setter



//Getter

int Auto::getradzahl(){return radzahl;}
int Auto::getps(){return ps;}
int Auto::getanzahl_sitze(){return anzahl_sitze;}
float Auto::gettankinhalt(){return tankinhalt;}
float Auto::gettankgroesse(){return tankgroesse;}
string Auto::getfarbe(){return farbe;}
float Auto::getspritverbrauch(){return spritverbrauch;}
