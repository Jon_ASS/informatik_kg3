#include "Datum.h"
#include "Datum.cpp"
#include <iostream>
#include <string>

using namespace std;

int main()
{
    cout << "IstSchaltjahr Test:" << endl;
    cout << "Jahr: 2015 -> Schaltjahr: " << (Datum::IstSchaltjahr(2015) == 0 ? "nein" : "ja") << endl;


    cout << "TagDesJahrs Test:" << endl;
    //Kein Schaltjahr
    Datum date = Datum(25, 04, 1993);
    cout << "25.04.1993 ist der: " << date.TagDesJahres() << " des Jahres." << endl;

    //Schaltjahr
    Datum date2 = Datum(25, 04, 1996);
    cout << "25.04.1996 ist der: " << date2.TagDesJahres() << " des Jahres." << endl;

    //Test: Mehr als 10 istanzen
    for (int i = 0; i < 10; i++)
    {
        Datum date3 = Datum(21, 1, 1998);
    }

    //Test Plappern
    Datum::Plappern(true);
    Datum date4 = Datum(25, 04, 1996);
    date4.TagDesJahres();

    Datum::Plappern(false);
    Datum date5 = Datum(25, 04, 1996);
    date5.TagDesJahres();

    return 0;
}

