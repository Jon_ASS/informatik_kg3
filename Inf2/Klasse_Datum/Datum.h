#ifndef DATUM_H
#define DATUM_H
#include<iostream>


class Datum
{
    private:
        int tag;
        int monat;
        int jahr;

    public:

        //allgemeiner Konstruktor
        Datum(int Tag, int Monat, int Jahr);
        ~Datum();

        //Methode
        static int IstSchaltjahr(int irgendeinJahr);
        int TagDesJahres();

        static void Plappern(bool flag);

};

#endif // DATUM_H


