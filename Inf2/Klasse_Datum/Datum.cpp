#include "Datum.h"
#include <iostream>
using namespace std;

static int wiederholungsZaehler = 0;
static bool plappern = false;

Datum::Datum(int Tag, int Monat, int Jahr)
{
    if (plappern){
		cout << "Obejekt (Addresse: " << this << ") erezugt " << endl;
	}
	wiederholungsZaehler++;
	if(wiederholungsZaehler > 10){
        cout << "Warnung : Mehr als 10 Instanzen vom Objekt Datum erzeugt." << endl;
	}

    tag = Tag;
    monat = Monat;
    jahr = Jahr;
};

Datum::~Datum(){
    if (plappern){
		cout << "Obejekt (Addresse: " << this << ") zerstört " << endl;
	}
};

void Datum::Plappern(bool flag){
    plappern = flag;
};


//Methoden
int Datum::IstSchaltjahr(int irgendeinJahr){
    if (irgendeinJahr % 4 == 0){
        if (irgendeinJahr % 100 == 0){
            if (irgendeinJahr % 400 == 0){
                return 1;
            }else{
            return 0;
            }
        }else{
        return 1;
        }
    }else{
    return 0;
    }
};

int Datum::TagDesJahres(){
    if (plappern){
		cout << "Obejekt (Addresse: " << this << ") Methode TagDesJahres aufgerufen " << endl;
	}
    int anzahltage = tag;
    if (monat > 2 && IstSchaltjahr(jahr) == 1 ){
        anzahltage += 1;
    }else{
        anzahltage += 0;
    }
    switch (monat){
    case 1: anzahltage += 0; break;
    case 2: anzahltage += 31; break;
    case 3: anzahltage += 59; break;
    case 4: anzahltage += 90; break;
    case 5: anzahltage += 120; break;
    case 6: anzahltage += 151; break;
    case 7: anzahltage += 181; break;
    case 8: anzahltage += 212; break;
    case 9: anzahltage += 243; break;
    case 10: anzahltage += 273; break;
    case 11: anzahltage += 304; break;
    case 12: anzahltage += 334; break;
    default: cout << "Monat stimmt nicht" << endl;
    }
    return anzahltage;
}


