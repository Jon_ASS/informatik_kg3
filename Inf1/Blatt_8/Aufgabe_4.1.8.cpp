#include<iostream>
#include<string>
#include<sstream>
using namespace std;

int main()
{
	int l;
	double laenge{ 100.05 };
	char zeichen;
	string messlatte { "777.77" };
	
	l = static_cast<int>(laenge); 
	zeichen = static_cast<char>(laenge); 
	laenge = stod(messlatte);
	
	cout << laenge << " " << l << " " << zeichen << endl;

	/* Das Programm gibt die Zeile "777.77 100 d" aus. Dabei wurde die Variable laenge, welche eigentlich oben als double mit dem Wert 100.05 definiert ist,
	mit dem konvertiertem Wert aus der Stringvariablen messlatte überschrieben.
	100 wird ausgegeben, weil sich dies aus der Konvertierung von einer Fließkommazahl zu einem ganzzahligem Integer ergibt. Die Kommastellen gehen verloren.
	In einem "echten" Programm sollte der Wert jedoch vor der Konvertierung ggf. noch sinnvoll mittels round() gerundet werden, da ansonsten immer abgerundet wird.
	Das kleine d wird ausgegeben, weil der Wert 100 in einem char beim Ausgeben vom Compiler als Code für einen Buchstaben interpretiert wird.
	100 steht in der hinterlegten (ASCII-)Tabelle für das kleine d, 68 das große D, 101 wäre bspw. das kleine e usw. (https://tools.piex.at/ascii-tabelle/) */

	return 0;
}