#include<iostream>
using namespace std;


int main()
{
	int a = 2;
	int b = 1;

	a = b = 2; // b wird der Wert 2 zugewiesen und dann wird a der Wert von b zugewiesen
	cout << a << endl;
	a = 5 * 3 + 2; // a wird der Wert 17 zugewiesen, da Punkt vor Strich Rechnung
	cout << a << endl;
	a = 5 * (3 + 2); // a wird der Wert 25 zugewiesen, da Klammern vor Punkt-Rechnungen gerechnet werden
	cout << a << endl;
	a *= 5 + 5; // a wird der Wert 250 zugewiesen, da a = a [25] * (5+5) gerechnet wird, [der Wert vorher in a]
	cout << a << endl;
	a %= 2 * 3; // a wird der Wert 4 zugewiesen, da a = a [250] % (2*3) gerechnet wird, bei %(modulo) wird der Rest einer Division ausgegeben 250 / 6 = 41 R 4
	cout << a << endl;
	a = !(--b == 0); // a wird der Wert 1 zugewiesen, da --b (b= b[2]-1) entspricht, dann überprüft wird ob dieses gleich null ist. Anschließend wird, wenn es nicht so ist (!) a true(bool) = 1 gesetzt 
	cout << a << endl;
	a = 0 && 0 + 2; // a wird der Wert 0 zugewiesen, da nur wenn die Kette (mit && verbunden) true ergibt den letzten Wert ausgibt
	cout << a << endl;
	a = b++ * 2; // a wird der Wert 2 zugewiesen, da b = b[1] + 1 erst nach der Rechnung (b[1] *2) ausgeführt wird 
	cout << a << endl;
	a = -5 - 5; // a wird der Wert -10 zugewiesen, int können auch negative Zahlen speichern
	cout << a << endl;
	a = -(+b++); // a wird der Wert -2 zugewiesen, da b[2] erst mal eins und dann (da außerhalb der Klammer) mal minus eins gerechnet wird (Vgl. Z.17)
	cout << a << endl;
	a = 5 == 5 && 0 || 1; // a wird der Wert eins zugewiesen, da ( true && true ) und somit true rauskommt und sommit der Wert ganz am Ende genommen wird
	cout << a << endl;
	a = ((((((b + b) * 2) + b) && b || b)) == b); // a wird der Wert 0 zugewiesen, da bei dieser Rechnung false herauskommen wird
	cout << a << endl;
	a = b + ++b; // hier wird b[3] während der Rechnung um eins erhöht somit steht dort 4+4 und a wird der Wert 8 zugewiesen
	cout << a << endl;
	a = sizeof(int) * sizeof(a); // a wird der Wert 16 zugewiesen, da in C++ die Größe einer Zahl vom Typ int 4 bytes beträgt
	cout << a << endl;
 	
 	return 0;
}
