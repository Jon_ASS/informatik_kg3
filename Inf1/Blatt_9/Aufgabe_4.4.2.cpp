int i, k, n;

i=101, k=5, n=10;
while (i>0) k = 2*k; // while fragt nach ob i größer als null ist. Da dies der Fall ist und i nicht kleiner wird ist das eine Endlosschleif, bei der bei jedem Durchlauf k*2 in k gespeichert wird

i=101, k=5, n=10;
while (i!=0) i = i-2; // while fragt nach ob i nicht gleich null ist, dann wird i-2 in i gespeichert und es wiederholt sich endlos, da bei dieser Rechnung die 0 übersprungen wird

i=101, k=5, n=10;
while (n!=i) { //  while fragt nach ob n ungleich i ist, dann wird i um eins erhöht und 2*i in n gespeichert. Dies läuft wieder unendlich weiter, da n und i immer ungleich bleiben.
	++i;
	n = 2 * i;
}
