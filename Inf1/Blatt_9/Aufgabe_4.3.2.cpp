#include<iostream>
using namespace std;
int main()
{
int i, j;
i = 0; j = -5;
if(i++ || j++) ++i; // if fragt nach true oder false, da i++ (+1 erst danch ausgeführt) = 0 (bool= false) ist wird j++ ausgeführt und i nochmal um eins erhöht
cout << i << ',' << j << endl;
i = 1; j = -5;
if(i++ || j++) ++i; // i++ = 1 (bool= true) somit wird j++ nicht ausgeführt. Dann wird i nochmal um eins erhöht
cout << i << ',' << j << endl;
i = 0; j = -5;
if(i++ && j++) ++i; // da hier i++ false ist wird dort abgebrochen und somit der wert von i nur um eins erhöht
cout << i << ',' << j << endl;
i = 1; j = -5;
if(i++ && j++) ++i; // hier ist i++ = true somit wird j um eins erhöht und i nochmal um eins wegen der if abfrage
cout << i << ',' << j << endl;
}

